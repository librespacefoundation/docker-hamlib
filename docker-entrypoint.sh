#!/bin/sh -e
#
# Hamlib Docker startup script
#
# Copyright (C) 2022-2023 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CONFIG_COMMON_VARS="
MODEL:model:str
SERIAL_SPEED:serial-speed:str
PORT:port:str
LISTEN_ADDR:listen-addr:str
SET_CONF:set-conf:str
SHOW_CONF:show-conf:bool
LIST:list:bool
DUMP_CAPS:dump-caps:bool
VERBOSE:verbose:bool
DEBUG_TIME_STAMPS:debug-time-stamps:bool
HELP:help:bool
VERSION:version:bool
"

CONFIG_RIG_VARS="
$CONFIG_COMMON_VARS
RIG_FILE:rig-file:str
PTT_FILE:ptt-file:str
DCD_FILE:dcd-file:str
PTT_TYPE:ptt-type:str
DCD_TYPE:dcd-type:str
CIVADDR:civaddr:str
VFO:vfo:bool
TWIDDLE_TIMEOUT:twiddle-timeout:bool
UPLINK:uplink:bool
"

CONFIG_ROT_VARS="
$CONFIG_COMMON_VARS
ROT_FILE:rot-file:str
SET_AZOFFSET:set-azoffset:str
SET_ELOFFSET:set-eloffset:str
"

vars_to_args() {
	for config_var in $1; do
		_type="${config_var##*:}"
		_config_var="${config_var%:*}"
		_option="${_config_var##*:}"
		_var="${_config_var%:*}"
		# shellcheck disable=SC1003
		_value=$(eval "echo \$$_var" | sed 's/'\''/'\''\\'\'\''/g')
		if [ -n "$_value" ]; then
			case $_type in
				str)
					printf -- " --%s='%s'" "$_option" "$_value"
					;;
				bool)
					case $_value in
						1|true|True|TRUE)
							printf -- " --%s" "$_option"
							;;
					esac
					;;
			esac
		fi
	done
}

command="$1"
case $command in
	rigctld)
		if [ $# -eq 1 ]; then
			eval exec "$command" "$(vars_to_args "$CONFIG_RIG_VARS")"
		fi
		;;
	rotctld)
		if [ $# -eq 1 ]; then
			eval exec "$command" "$(vars_to_args "$CONFIG_ROT_VARS")"
		fi
		;;
	'')
		exec /bin/bash
		;;
esac

exec "$@"
