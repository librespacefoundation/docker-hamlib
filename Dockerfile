# Hamlib image
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG DEBIAN_IMAGE_TAG=bullseye
FROM debian:${DEBIAN_IMAGE_TAG}
LABEL org.opencontainers.image.authors='Vasilis Tsiligiannis <acinonyx@openwrt.gr>'

ARG HAMLIB_PACKAGE_VERSION
ARG HAMLIB_UID=999

# Add unprivileged system user
RUN groupadd -r -g ${HAMLIB_UID} hamlib \
	&& useradd -r -u ${HAMLIB_UID} \
		-g hamlib \
		-d /nonexistent \
		-s /bin/false \
		-G dialout,plugdev \
		hamlib

# Install system packages
RUN apt-get update \
	&& apt-get install -qy libhamlib-utils${HAMLIB_PACKAGE_VERSION:+=${HAMLIB_PACKAGE_VERSION}} \
	&& rm -r /var/lib/apt/lists/*

# Add container entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
