# Hamlib Docker image #

Hamlib Docker image.
This image includes Hamlib Amateur Radio Equipment Control libraries and utilities.

GitLab: [docker-hamlib](https://gitlab.com/librespacefoundation/docker-hamlib)
Docker Hub: [librespace/hamlib](https://hub.docker.com/r/librespace/hamlib/)
